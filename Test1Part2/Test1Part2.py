# Program Name:         Test1Part2.py
# Course:               IT1113/Section W01
# Student Name:         Kathryn Browning
# Assignment Number:    Test1Part2
# Due Date:             02/18/2018

primary_color = ''
primary_color_one = ''
primary_color_two = ''
secondary_color = ''

# This program prompts the user to enter two different primary colors, then mixes 
# those colors together to produce a secondary color.
def main():
    print('\nWelcome! The primary colors are RED, BLUE, and YELLOW.\nTo create a secondary color, you will need to pick 2 primary colors to mix!')

    while True:
        first_primary_color_input = input('\nPlease enter the name of one primary color:\n')
        primary_color_one = determine_color_input(first_primary_color_input)

        second_primary_color_input = input('\nPlease enter the name of a primary color different from your first selction:\n')
        primary_color_two = determine_color_input(second_primary_color_input)

        mix_colors(primary_color_one, primary_color_two)

        continue_program = input('\nWould you like to create more secondary colors? (y/n): ')
        if continue_program != 'y':
            break

# Figure out what primary colors the user entered
def determine_color_input(color_input):
    if color_input.lower() == 'red':
        primary_color = 'red'
        print('\nYou\'ve selected the primary color: ' + primary_color)
        return primary_color
    elif color_input.lower() == 'blue':
        primary_color = 'blue'
        print('\nYou\'ve selected the primary color: ' + primary_color)
        return primary_color
    elif color_input.lower() == 'yellow':
        primary_color = 'yellow'
        print('\nYou\'ve selected the primary color: ' + primary_color)
        return primary_color
    else:
       print('\nInvalid primary color option! The choices are RED, BLUE, or YELLOW.')

# Mix the primary colors and print results of secondary color
def mix_colors(first_color, second_color):
    if (first_color == 'red' and second_color == 'blue') or (first_color == 'blue' and second_color == 'red'):
        secondary_color = 'purple'
        print('\nThe secondary color created is: ' + secondary_color)
    elif (first_color == 'red' and second_color == 'yellow') or (first_color == 'yellow' and second_color == 'red'):
        secondary_color = 'orange'
        print('\nThe secondary color created is: ' + secondary_color)
    elif (first_color == 'blue' and second_color == 'yellow') or (first_color == 'yellow' and second_color == 'blue'):
        secondary_color = 'green'
        print('\nThe secondary color created is: ' + secondary_color)
    else:
        print('Please select two different primary colors!')

# Call main function
main()